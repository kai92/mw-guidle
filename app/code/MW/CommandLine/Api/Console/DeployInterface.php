<?php
namespace MW\CommandLine\Api\Console;
interface DeployInterface {

    /**
     * @return null
     */
    public function mwDeploy($deployName, $output);

}