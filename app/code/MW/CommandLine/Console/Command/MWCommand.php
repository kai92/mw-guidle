<?php

namespace MW\CommandLine\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MWCommand extends Command {

    protected $mwDeployInterface;

    public function __construct(
        \MW\CommandLine\Api\Console\DeployInterface $mwDeployInterface
    )
    {
        parent::__construct();
        $this->mwDeployInterface = $mwDeployInterface;
    }

    protected function configure()
    {
        $this->setName('mw:deploy');
        $this->setDescription('Deploy the app');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $deployCommand =  '';
            $this->mwDeployInterface->mwDeploy($deployCommand, $output);
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>Error: %s</error>', $e->getMessage()));
        }
    }
}