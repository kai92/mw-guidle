<?php
namespace MW\CommandLine\Model\Deploy;

class DeployService implements \MW\CommandLine\Api\Console\DeployInterface
{
    /**
     * @return $this
     * @throws \Exception
     */
    public function execute()
    {
        /** deploy acidpos */
        try {
            /** CODING YOUR PROGRAM HERE */
        }catch (\Exception $e){
            return false;
        }
        return true;
    }
    /**
     * @param $indexName
     * @param $output
     * @return null|string
     */
    public function mwDeploy($deployName, $output){
        try {
            $startTime = microtime(true);
            $result = $this->execute();
            $resultTime = microtime(true) - $startTime;
            $resultTime = round($resultTime,2).'s';
            $output->writeln('Hello World!');
            $messageSuccess = $result ? " Deploy Successfully Completed!!" : "";
            $output->writeln($messageSuccess);
            $output->writeln('Execution time : '.$resultTime);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return($e->getMessage());
        } catch (\Exception $e) {
            return($e->getMessage());
        }
    }
}