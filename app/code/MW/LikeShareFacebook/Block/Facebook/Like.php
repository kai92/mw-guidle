<?php

namespace MW\LikeShareFacebook\Block\Facebook;

class Like extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Cms\Model\Page
     */
    protected $_cmsPage;

    /**
     * @var \MW\LikeShareFacebook\Helper\Data
     */
    protected $_dataHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Cms\Model\Page $cmsPage
     * @param \MW\LikeShareFacebook\Helper\Data $dataHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Cms\Model\Page $cmsPage,
        \MW\LikeShareFacebook\Helper\Data $dataHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_urlBuilder = $context->getUrlBuilder();
        $this->_coreRegistry = $coreRegistry;
        $this->_cmsPage = $cmsPage;
        $this->_dataHelper = $dataHelper;
    }

    /**
     * Get current URL
     *
     * @return string
     */
    public function getCurrentUrl()
    {
        return $this->_urlBuilder->getCurrentUrl();
    }

    /**
     * Get site name
     *
     * @return string
     */
    public function getSiteName()
    {
        return $this->_dataHelper->getStoreConfig('general/store_information/name');
    }

    /**
     * Get Facebook App ID
     *
     * @return string
     */
    public function getAppId()
    {
        $appId = $this->_dataHelper->getFacebookLikeAppId();
        return $appId;
    }

    /**
     * @return \Magento\Framework\Registry
     */
    public function getRegistry()
    {
        return $this->_coreRegistry;
    }

    /**
     * @return \Magento\Cms\Model\Page
     */
    public function getCmsPage()
    {
        return $this->_cmsPage;
    }

    public function getCurrentProduct()
    {
        return $this->getRegistry()->registry('current_product');
    }

    public function getPageUrl(){
        return $this->getCurrentProduct()->getProductUrl();
    }
}
