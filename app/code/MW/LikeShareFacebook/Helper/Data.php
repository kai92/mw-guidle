<?php

namespace MW\LikeShareFacebook\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	protected $_scopeConfig;
	protected $_config;
    protected $_storeManager;

	public function __construct(
		\Magento\Framework\App\Helper\Context $context,
		\Magento\Config\Model\ResourceModel\Config $config,
        \Magento\Store\Model\StoreManagerInterface $storeManager
	) {
		parent::__construct($context);
		$this->_config = $config;
		$this->_scopeConfig = $context->getScopeConfig();
		$this->_storeManager = $storeManager;
	}

	public function getStoreConfig($xmlPath, $storeCode = null)
	{
		if ($storeCode != null) {
			return $this->_scopeConfig->getValue(
				$xmlPath,
				'stores',
				$storeCode
			);
		} else {
			return $this->_scopeConfig->getValue(
				$xmlPath,
				'stores'
			);
		}
	}

    public function getCurrentStoreView()
    {
        try{
            return $this->_storeManager->getDefaultStoreView();
        }catch (\Exception $e){
            return $this->_storeManager->getStore();
        }
    }

	public function getFacebookLikeAppId()
	{
	    $store = $this->getCurrentStoreView();
	    $storeId = $store->getId();
		return $this->getStoreConfig('mwfacebook/facebook/appid', $storeId);
	}
}
