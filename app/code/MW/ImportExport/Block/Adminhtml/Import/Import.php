<?php

namespace MW\ImportExport\Block\Adminhtml\Import;
class Import extends \Magento\Backend\Block\Widget\Form\Container
{
    public function _construct()
    {
        parent::_construct();
        $this->_blockGroup = 'MW_ImportExport';
        $this->_controller = 'adminhtml_import';
        $this->_mode = 'import';
        $this->buttonList->update('save', 'label', __('Import'));
        $this->buttonList->remove('delete');
        $this->buttonList->remove('reset');
    }
}