<?php
namespace MW\ImportExport\Block\Adminhtml\Import;
class Instruction extends \Magento\Backend\Block\Template
{
    public $isProductImport = false;

    /**
     * Get adjust stock csv sample link
     *
     * @return mixed
     */
    public function getCsvSampleLink() {
        $url = $this->getUrl('export/product/downloadsample');
        return $url;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return 'Please choose a CSV file to import Stock. You can download this sample CSV file';
    }

}