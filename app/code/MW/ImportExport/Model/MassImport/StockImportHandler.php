<?php

namespace MW\ImportExport\Model\MassImport;

use Magento\Framework\App\Filesystem\DirectoryList;
use Acidpos\Inventory\Api\Data\IM\AdjustStock\AdjustStockInterface;

class StockImportHandler
{
    protected $csvProcessor;
    protected $messageManager;
    protected $customImportStockResourceModel;
    public function __construct(
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \MW\ImportExport\Model\ResourceModel\CustomImportStock $customImportStockResourceModel
    )
    {
        $this->csvProcessor = $csvProcessor;
        $this->messageManager = $messageManager;
        $this->customImportStockResourceModel = $customImportStockResourceModel;
    }

    public function importProducts($productsImport){
        try {
            $this->customImportStockResourceModel->insert($productsImport);
            return array(
                'import_success' => count($productsImport),
            );
        }catch (\Exception $e){
            return array(
                'import_success' => 0,
                'error' => $e->getMessage()
            );
        }
    }

    public function importFromCsvFile($file)
    {
        if (!isset($file['tmp_name'])) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid file upload attempt.'));
        }
        $importProductRawData = $this->csvProcessor->getData($file['tmp_name']);
        $fileFields = $importProductRawData[0];
        $validFields = $this->_filterFileFields($fileFields);
        $invalidFields = array_diff_key($fileFields, $validFields);
        $importProductData = $this->_filterImportProductData($importProductRawData, $invalidFields, $validFields);
        if (!count($importProductData)) {
            return array(
                'import_success' => 0,
                'error' => 'the import file is invalid'
            );
        }
        $header = array();
        $productsImport = array();
        $result = array();
        foreach ($importProductData as $rowIndex => $dataRow) {
            // skip headers
            if ($rowIndex == 0) {
                $header = $dataRow;
                continue;
            }
            $countCols = count($dataRow);
            $newProduct = [];
            for ($i = 0; $i < $countCols; $i++ ){
                if(isset($dataRow[$i]) && $dataRow[$i] != '') {
                    $newProduct[$header[$i]] = $dataRow[$i];
                }
            }
            if(count($newProduct) > 0){
                $productsImport[] = $newProduct;
            }
            if(count($productsImport) >= 300 ){
                $result = $this->importProducts($productsImport);
                $productsImport = [];
            }
        }
        if(count($productsImport) > 0 ){
            $result = $this->importProducts($productsImport);
        }
        return $result;
    }

    /**
     * Filter file fields (i.e. unset invalid fields)
     *
     * @param array $fileFields
     * @return string[] filtered fields
     */
    protected function _filterFileFields(array $fileFields)
    {
        $filteredFields = $this->getRequiredCsvFields();
        $requiredFieldsNum = count($this->getRequiredCsvFields());
        $fileFieldsNum = count($fileFields);

        // process title-related fields that are located right after required fields with store code as field name)
        for ($index = $requiredFieldsNum; $index < $fileFieldsNum; $index++) {
            $titleFieldName = $fileFields[$index];
            $filteredFields[$index] = $titleFieldName;
        }
        return $filteredFields;
    }

    public function getRequiredCsvFields()
    {
        // indexes are specified for clarity, they are used during import
        return [
            0 => __('sku'),
        ];
    }

    protected function _filterImportProductData(array $productRawData, array $invalidFields, array $validFields)
    {
        foreach ($productRawData as $rowIndex => $dataRow) {
            // skip empty rows
            if (count($dataRow) <= 1) {
                unset($productRawData[$rowIndex]);
                continue;
            }
            // unset invalid fields from data row
            foreach ($dataRow as $fieldIndex => $fieldValue) {
                if (isset($invalidFields[$fieldIndex])) {
                    unset($productRawData[$rowIndex][$fieldIndex]);
                }
            }
        }
        return $productRawData;
    }
}
