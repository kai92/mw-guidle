<?php

namespace MW\ImportExport\Model\ResourceModel;

class CustomImportStock
{
    const TABLE_NAME = "custom_import_export_stock";
    protected $resourceConnection;
    protected $tableName;

    public function __construct (
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->tableName = $this->resourceConnection->getTableName(self::TABLE_NAME);
    }

    public function insert($insertData)
    {
        $connection = $this->resourceConnection->getConnection();
        try{
            $connection->beginTransaction();
            if($insertData) {
                $connection->insertMultiple($this->tableName, $insertData);
            }
            $connection->commit();
            return true;
        }catch (\Exception $e){
            $connection->rollBack();
        }
    }
}
