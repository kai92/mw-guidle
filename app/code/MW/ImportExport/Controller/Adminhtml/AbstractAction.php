<?php

namespace MW\ImportExport\Controller\Adminhtml;

abstract class AbstractAction extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $customerFactory;

    public function __construct(
        \MW\ImportExport\Controller\Adminhtml\Context $context
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $context->getResultPageFactory();
    }
}