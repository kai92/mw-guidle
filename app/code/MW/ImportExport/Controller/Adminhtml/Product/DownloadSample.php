<?php
namespace MW\ImportExport\Controller\Adminhtml\Product;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\File\Csv;

class DownloadSample extends \Magento\Backend\App\Action
{
    protected $generated = array();
    protected $fileFactory;
    protected $filesystem;
    protected $csvProcessor;
    protected $fileWriteFactory;
    protected $driverFile;
    protected $date;

    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        Filesystem $filesystem,
        Csv $csvProcessor,
        \Magento\Framework\Filesystem\File\WriteFactory $fileWriteFactory,
        \Magento\Framework\Filesystem\Driver\File $driverFile,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        parent::__construct($context);
        $this->fileFactory = $fileFactory;
        $this->filesystem = $filesystem;
        $this->csvProcessor = $csvProcessor;
        $this->fileWriteFactory = $fileWriteFactory;
        $this->driverFile = $driverFile;
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        $name = md5(microtime());
        $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR)->create('import');
        $filename = DirectoryList::VAR_DIR.'/import/'.$name.'.csv';

        $stream = $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR)->openFile($filename, 'w+');
        $stream->lock();
        $dataHeader = array(
            array('sku', 'product_name', 'description', 'qty', 'price', 'status')
        );
        $random1 = rand();
        $random2 = rand();
        $dataBody = array(
            array('simple-'.$random1, 'simple-name-'.$random1, 'description1', 100, 200, 1),
            array('simple-'.$random2, 'simple-name-'.$random2, 'description2', 50, 100, 1)
        );

        $data = array_merge($dataHeader, $dataBody);
        foreach ($data as $row) {
            $stream->writeCsv($row);
        }
        $stream->unlock();
        $stream->close();

        return $this->fileFactory->create(
            'import_stock_sample.csv',
            array(
                'type' => 'filename',
                'value' => $filename,
                'rm' => true  // can delete file after use
            ),
            DirectoryList::VAR_DIR
        );
    }
}
