<?php

namespace MW\ImportExport\Controller\Adminhtml\Product;

class Index extends \MW\ImportExport\Controller\Adminhtml\AbstractAction

{

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('MW_ImportExport::importexport');
        $resultPage->getConfig()->getTitle()->prepend(__('Import Export Stock'));
        $resultPage->addBreadcrumb(__('Import Export Stock'), __('Import Export Stock'));
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('MW_ImportExport::importexport');
    }

}
