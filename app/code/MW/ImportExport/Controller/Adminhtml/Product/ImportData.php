<?php

namespace MW\ImportExport\Controller\Adminhtml\Product;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\File\Csv;


class ImportData extends \Magento\Backend\App\Action
{
    /**
     * @var array
     */
    protected $generated = array();

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var Csv
     */
    protected $csvProcessor;

    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        Filesystem $filesystem,
        Csv $csvProcessor
    ) {
        parent::__construct($context);
        $this->fileFactory = $fileFactory;
        $this->filesystem = $filesystem;
        $this->csvProcessor = $csvProcessor;
    }

    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        if ($this->getRequest()->isPost()) {
            try {
                $importHandler = $this->_objectManager->create('MW\ImportExport\Model\MassImport\StockImportHandler');
                $result = $importHandler->importFromCsvFile($this->getRequest()->getFiles('file_csv'));
                $importSuccess = $result['import_success'];

                if(isset($result['error'])){
                    $this->messageManager->addErrorMessage($result['error']);
                    return $resultRedirect->setPath('*/*/index');
                }

                if ($importSuccess) {
                    $this->messageManager->addSuccessMessage(__('The Product(s) has been imported.'));
                    return $resultRedirect->setPath("*/*/index");
                }

            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        } else {
            $this->messageManager->addErrorMessage(__('Invalid file upload attempt'));
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect->setUrl($this->_redirect->getRedirectUrl());
        return $resultRedirect;

    }
}
