<?php

namespace MW\UiGrid\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\Pricing\PriceCurrencyInterface;


class EmailCustom extends Column
{
    protected $backendUrl;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        array $components = [],
        array $data = []
    ) {
        $this->backendUrl = $backendUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $memberId = isset($item['entity_id']) ? $item['entity_id'] : null;
                $email = isset($item['email']) ? $item['email'] : null;
                $item[$this->getData('name')] = $this->getLinkHtml($memberId,$email);
            }
        }
        return $dataSource;
    }

    public function getLinkHtml($memberId, $email){
        $result = __(
            '<b><a href="%1">%2</a></b>',
            $this->backendUrl->getUrl('affilex/member_active/edit', ['id' => $memberId]),
            $email
        );
        return $result;
    }
}
