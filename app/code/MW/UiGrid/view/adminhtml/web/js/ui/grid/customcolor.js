define([
    'underscore',
    'Magento_Ui/js/grid/columns/column'
], function (_, Column) {
    'use strict';

    return Column.extend({
        defaults: {
            bodyTmpl: 'MW_UiGrid/grid/customcolor'
        },

        getColor: function (row) {
            if (row.entity_id % 2 === 0) {
                return '#0bff27';
            }
            return '#ee1e0e';
        }
    });
});