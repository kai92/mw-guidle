<?php

namespace MW\UiGrid\Controller\Adminhtml\Customer;

class Index extends \MW\UiGrid\Controller\Adminhtml\AbstractAction

{
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('MW_UiGrid::customer_listing');
        $resultPage->getConfig()->getTitle()->prepend(__('Customer Listing'));
        $resultPage->addBreadcrumb(__('Customer Listing'), __('Customer Listing'));
        return $resultPage;
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('MW_UiGrid::customer_listing');
    }
}
