<?php

namespace MW\UiGrid\Controller\Adminhtml;

abstract class AbstractAction extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $customerFactory;

    public function __construct(
        \MW\UiGrid\Controller\Adminhtml\Context $context
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $context->getResultPageFactory();
    }
}