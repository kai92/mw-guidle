<?php
namespace MW\UiGrid\Model\ResourceModel\Customer\UiGrid;

use Magento\Customer\Ui\Component\DataProvider\Document;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Psr\Log\LoggerInterface as Logger;

class Collection extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{
    protected $document = Document::class;
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        $mainTable = 'customer_grid_flat',
        $resourceModel = 'Magento\Customer\Model\ResourceModel\Customer'
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
    }

    public function getData(){
        $data = parent::getData();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $request = $objectManager->create('Magento\Framework\App\RequestInterface');
        $action  = $request->getControllerName();
        if($action == "export") {
            foreach ($data as &$item) {
                if(isset($item['status'])) {
                    $item['status'] = $item['status'] ? "Enabled" : "Disabled";
                }
            }
        }
        return $data;
    }
}
