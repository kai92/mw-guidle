<?php

namespace MW\Indexer\Model\ResourceModel\Indexer\Statistics;

use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Indexer\Table\StrategyInterface;

abstract class AbstractResource extends \Magento\Indexer\Model\ResourceModel\AbstractResource
{
    public function __construct(
        Context $context,
        StrategyInterface $tableStrategy,
        $connectionName = null
    ) {
        parent::__construct($context, $tableStrategy, $connectionName);
    }

    abstract protected function process();

    public function reindexAll()
    {
        $this->tableStrategy->setUseIdxTable(true);
        $this->clearTemporaryIndexTable();
        $this->beginTransaction();
        try {
            $this->process();
            $this->commit();
        } catch (\Exception $e) {
            $this->rollBack();
            throw $e;
        }
        $this->syncData();
        return $this;
    }
 
    public function clearTemporaryIndexTable()
    {
        $this->getConnection()->truncateTable($this->getIdxTable());
    }

    protected function safeInsertFromSelect($select, $table, $fields)
    {
        $connection = $this->getConnection();
        $connection->query('SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;');
        $connection->query($select->insertFromSelect($table, $fields));
    }

}
