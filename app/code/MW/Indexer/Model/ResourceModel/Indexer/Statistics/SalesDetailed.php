<?php
namespace MW\Indexer\Model\ResourceModel\Indexer\Statistics;

class SalesDetailed extends AbstractResource
{
    protected function _construct()
    {
        $this->_init('clone_sales_order', 'id');
    }

    protected function process()
    {
        $columns = $this->getColumns();

        $orderItemTable = $this->getTable('sales_order_item');
        $select = $this->getConnection()->select()
            ->from(['main_table' => $this->getTable('sales_order')], [])
            ->join(
                ['item' => $orderItemTable],
                '(item.order_id = main_table.entity_id AND item.parent_item_id IS NULL)',
                []
            )->joinLeft(
                ['item2' => $orderItemTable],
                '(item2.order_id = main_table.entity_id AND item2.parent_item_id IS NOT NULL AND '
                . 'item2.parent_item_id = item.item_id AND item.product_type IN ("configurable", "bundle"))',
                []
            )->columns($columns)
            ->group('item.item_id');

        $this->safeInsertFromSelect($select, $this->getIdxTable(), array_keys($columns));
    }

    private function getColumns()
    {
        $columns = [
            'store_id' => 'main_table.store_id',
            'order_status' => 'main_table.status',
            'order_id' => 'main_table.entity_id',
            'product_id' => 'IFNULL(item.product_id, item2.product_id)',
            'product_name' => 'item.name'
        ];

        return $columns;
    }
}
