<?php
namespace MW\Indexer\Model;
class Flag extends \Magento\Framework\Flag
{

    const MW_DATA_STATISTICS_FLAG_CODE = 'mw_data_statistics';

    public function setReportFlagCode($code)
    {
        $this->_flagCode = $code;
        return $this;
    }
}
