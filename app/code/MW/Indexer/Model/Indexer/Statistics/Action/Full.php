<?php
namespace MW\Indexer\Model\Indexer\Statistics\Action;


class Full extends \MW\Indexer\Model\Indexer\Statistics\AbstractAction
{

    public function execute($ids = null)
    {
        try {
            $this->reindexAll();
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()), $e);
        }
    }
}
