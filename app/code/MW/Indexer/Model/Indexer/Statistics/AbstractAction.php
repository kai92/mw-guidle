<?php
namespace MW\Indexer\Model\Indexer\Statistics;

use MW\Indexer\Model\ResourceModel\Indexer\Statistics;
use MW\Indexer\Model\Flag;
use MW\Indexer\Model\FlagFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\ObjectManagerInterface;

abstract class AbstractAction
{
    private $objectManager;

    private $reportsFlag;

    private $dateTime;

    private $resourceModelNames = [
        'sales_detailed' => Statistics\SalesDetailed::class
    ];

    public function __construct(
        FlagFactory $reportsFlagFactory,
        DateTime $dateTime,
        ObjectManagerInterface $objectManager
    ) {
        $this->reportsFlag = $reportsFlagFactory->create();
        $this->dateTime = $dateTime;
        $this->objectManager = $objectManager;
    }

    abstract public function execute($ids);

    public function reindexAll()
    {
        $this->setFlagData(Flag::MW_DATA_STATISTICS_FLAG_CODE);
        foreach ($this->resourceModelNames as $resourceModelName) {
            $model = $this->objectManager->create($resourceModelName);
            $model->reindexAll();
        }
    }

    private function setFlagData($code, $value = null)
    {
        $this->reportsFlag->setReportFlagCode($code)->unsetData()->loadSelf();
        if ($value !== null) {
            $this->reportsFlag->setFlagData($value);
        }
        $this->reportsFlag->setLastUpdate($this->dateTime->gmtDate());
        $this->reportsFlag->save();
        return $this;
    }
}
