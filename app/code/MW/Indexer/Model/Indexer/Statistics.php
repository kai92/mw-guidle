<?php
namespace MW\Indexer\Model\Indexer;


class Statistics implements \Magento\Framework\Indexer\ActionInterface, \Magento\Framework\Mview\ActionInterface
{

    private $statisticsIndexerFull;


    public function __construct(
        Statistics\Action\Full $statisticsIndexerFull
    ) {
        $this->statisticsIndexerFull = $statisticsIndexerFull;
    }


    public function executeFull()
    {
        $this->statisticsIndexerFull->execute();
    }


    public function execute($ids)
    {
    }


    public function executeList(array $ids)
    {
    }


    public function executeRow($id)
    {
    }
}
